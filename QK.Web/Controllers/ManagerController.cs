﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QK.Framework;
using QK.Model.Search;

namespace QK.Web.Controllers
{
    public class ManagerController : BaseController
    {
        private readonly BLL.ManagerBll managerService = new BLL.ManagerBll();
        /// <summary>
        /// 分页数据列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="sortname"></param>
        /// <param name="sortorder"></param>
        /// <returns></returns>
        public JsonResult GetPagedList(ManagerField field, int page = 1, int pagesize = 30, string sortname = ""
            , string sortorder = "")
        {
            page = page <= 0 ? 1 : page;
            pagesize = pagesize <= 0 ? 30 : pagesize;
            string filedOrder = string.Concat(sortname, " ", sortorder);
            if (string.IsNullOrEmpty(filedOrder))
            {
                filedOrder = " id desc";
            }

            return JsonMsg(managerService.GetPagedList(pagesize, page, "", filedOrder, field));
        }
    }
}