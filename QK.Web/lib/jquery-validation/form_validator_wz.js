﻿//验证提示显示
var defaultValidateErrorPlacement = function (lable, element) {

    if (element.hasClass("l-textarea")) {
        element.ligerTip({ content: lable.html(), target: element[0] });
    }
    else if (element.hasClass("l-text-field")) {
        element.parent().ligerTip({ content: lable.html(), target: element[0] });
    }
    else {
        lable.appendTo(element.parents("td:first").next("td"));
    }

};

var defaultValidateSuccess = function (lable) {
    lable.ligerHideTip();
    lable.remove();

};

var deafultValidate = function (validateElements) {
    return validateElements.validate({
        invalidHandler: function (e, validator) {
            //parent.parent.jsprint("有 " + validator.numberOfInvalids() + " 项填写有误，请检查！", "", "Warning");
        },
        errorPlacement: function (lable, element) {
            //可见元素显示错误提示
            defaultValidateErrorPlacement(lable, element);
        },
        success: function (lable) {
            defaultValidateSuccess(lable);
        }
    });
};