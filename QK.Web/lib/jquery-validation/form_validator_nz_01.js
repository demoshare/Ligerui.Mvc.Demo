﻿//验证提示显示
var defaultValidateErrorPlacement = function (lable, element) {

    if (element.hasClass("l-textarea")) {
        element.addClass("l-textarea-invalid");
    }
    else if (element.hasClass("l-text-field")) {
        element.parent().addClass("l-text-invalid");
    }
    $(element).removeAttr("title").ligerHideTip();
    $(element).attr("title", lable.html()).ligerTip();


};

var defaultValidateSuccess = function (lable) {
    var element = $("#" + lable.attr("for"));
    if (element.hasClass("l-textarea")) {
        element.removeClass("l-textarea-invalid");
    }
    else if (element.hasClass("l-text-field")) {
        element.parent().removeClass("l-text-invalid");
    }
    $(element).removeAttr("title").ligerHideTip();


};

var deafultValidate = function (validateElements) {
    return validateElements.validate({
        invalidHandler: function (e, validator) {
            parent.parent.jsprint("有 " + validator.numberOfInvalids() + " 项填写有误，请检查！", "", "Warning");
        },
        errorPlacement: function (lable, element) {
            //可见元素显示错误提示
            defaultValidateErrorPlacement(lable, element);
        },
        success: function (lable) {
            defaultValidateSuccess(lable);
        }
    });
};