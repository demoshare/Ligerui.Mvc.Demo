﻿//内置验证
var defaultValidateErrorPlacement = function (lable, element) {

    if (element.hasClass("l-textarea")) {
        element.addClass("l-textarea-invalid");
    }
    else if (element.hasClass("l-text-field")) {
        element.parent().addClass("l-text-invalid");
    }

    var nextCell = element.parents("td:first").next("td");
    nextCell.find("div.l-exclamation").remove();
    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip();

};

var defaultValidateSuccess = function (lable) {
    var element = $("#" + lable.attr("for"));
    var nextCell = element.parents("td:first").next("td");
    if (element.hasClass("l-textarea")) {
        element.removeClass("l-textarea-invalid");
    }
    else if (element.hasClass("l-text-field")) {
        element.parent().removeClass("l-text-invalid");
    }
    nextCell.find("div.l-exclamation").remove();

};

var deafultValidate = function (validateElements) {
    return validateElements.validate({
        invalidHandler: function (e, validator) {
            parent.parent.jsprint("有 " + validator.numberOfInvalids() + " 项填写有误，请检查！", "", "Warning");
        },
        errorPlacement: function (lable, element) {
            //可见元素显示错误提示
            defaultValidateErrorPlacement(lable, element);
        },
        success: function (lable) {
            defaultValidateSuccess(lable);
        }
    });
};