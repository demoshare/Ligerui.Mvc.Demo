﻿//禁止按回车键提交表单
document.onkeypress = function () {
    if (event.keyCode == 13) {
        return false;
    }
}
//获取url地址指定参数值
var GetUrlParam = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

//获取页面功能节点
var SetButtons = function (toolbar, url) {   
    var buttons = [];
    var arr = [
        { text: '新增', click: f_btnClick, icon: 'add' },
        { text: '修改', click: f_btnClick, icon: 'modify' },
        { text: '删除', click: f_btnClick, icon: 'delete' },
        { text: '查询', click: f_btnClick, icon: 'search' },
        { text: '刷新', click: f_btnClick, icon: 'refresh' },
        { text: '导出', click: f_btnClick, icon: 'export' }
    ];
    for (var i = 0; i < arr.length; i++) {
        buttons.push(arr[i]);
    }

    toolbar.ligerToolBar({ items: buttons });
};


//判断
function f_btnClick(item) {
    switch (item.icon) {
        case "add":
            f_add();
            break;
        case "modify":
            f_modify();
            break;
        case "delete":
            f_delete();
            break;
        case "search":
            f_search();
            break;
        case "export":
            f_excel();
            break;
        case "refresh":
            f_reload();
            break;
    }
}

//格式化日期（例如：/Date(1490004552000)/）
function ChangeDateFormat(cellval) {

    var date = new Date(parseInt(cellval.replace("/Date(", "").replace(")/", ""), 10));

    return date.format("yyyy-MM-dd hh:mm:ss")
}


Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month 
        "d+": this.getDate(), //day 
        "h+": this.getHours(), //hour 
        "m+": this.getMinutes(), //minute 
        "s+": this.getSeconds(), //second 
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter 
        "S": this.getMilliseconds() //millisecond 
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}