﻿using QK.Model.Expand;
using QK.Model.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.BLL
{
     /// <summary>
    /// 数据访问层:管理员信息
    /// </summary>
    public partial class ManagerBll
    {
        private readonly DAL.ManagerDal dal = new DAL.ManagerDal();
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        /// <returns></returns>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 查询用户名是否存在
        /// </summary>
        public bool Exists(string userName)
        {
            return dal.Exists(userName);
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Model.Manager model)
        {
            return dal.Add(model);
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.Manager model)
        {
            return dal.Update(model);
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Model.Manager GetModel(int ID)
        {
            return dal.GetModel(ID);

        }


        /// <summary>
        /// 根据用户名密码返回一个实体
        /// </summary>
        public Model.Manager GetModel(string userName, string userPwd)
        {
            return dal.GetModel(userName, userPwd);

        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<Model.Manager> GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public IList<Model.Manager> GetList(int top, string strWhere, string filedOrder)
        {
            return dal.GetList(top, strWhere, filedOrder);
        }

        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public PagedList<IList<Model.Manager>> GetPagedList(int pageSize, int pageIndex, string strWhere, string filedOrder, ManagerField field)
        {
            return dal.GetPagedList(pageSize, pageIndex, strWhere, filedOrder, field);
        }
    }

}
