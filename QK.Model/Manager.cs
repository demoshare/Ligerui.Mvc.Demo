﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Model
{
    /// <summary>
    /// 系统管理员实体
    /// </summary>
    [Serializable]
    public class Manager
    {
        /// <summary>ID
        /// </summary>		
        public int ID { get; set; }

        /// <summary>角色编号
        /// </summary>		
        public int RoleId { get; set; }

        /// <summary>角色类型
        /// </summary>		
        public int RoleType { get; set; }

        /// <summary>真实姓名
        /// </summary>		
        public string RealName { get; set; }

        /// <summary>用户名称
        /// </summary>		
        public string UserName { get; set; }

        /// <summary>用户密码
        /// </summary>		
        public string UserPwd { get; set; }

        /// <summary>手机号码
        /// </summary>		
        public string Telephone { get; set; }

        /// <summary>邮箱
        /// </summary>		
        public string Email { get; set; }

        /// <summary>是否锁定：0否，1是
        /// </summary>		
        public bool IsLock { get; set; }

        /// <summary>添加时间
        /// </summary>		
        public DateTime AddTime { get; set; }

    }
}
