﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.Framework
{
    public enum BusinessCode
    {

        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,

        /// <summary>
        /// 统一错误
        /// </summary>
        UnifiedError = 1001,

        /// <summary>
        /// 参数错误
        /// </summary>
        ParameterError = 1000,

    }

}
