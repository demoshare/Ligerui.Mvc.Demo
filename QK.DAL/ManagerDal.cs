﻿using MySql.Data.MySqlClient;
using QK.Common;
using QK.DBUtility;
using QK.Model.Expand;
using QK.Model.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QK.DAL
{
    /// <summary>
    /// 数据访问层:管理员信息
    /// </summary>
    public partial class ManagerDal
    {
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        /// <returns></returns>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from manager");
            strSql.Append(" where ");
            strSql.Append(" ID = @ID  ");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@ID", MySqlDbType.Int32,11)         };
            parameters[0].Value = ID;

            return DbHelperMySQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 查询用户名是否存在
        /// </summary>
        public bool Exists(string userName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from manager");
            strSql.Append(" where user_name=@UserName ");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@UserName", MySqlDbType.VarChar,50)};
            parameters[0].Value = userName;

            return DbHelperMySQL.Exists(strSql.ToString(), parameters);
        }



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Model.Manager model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into manager(");
            strSql.Append("RoleId,RoleType,RealName,UserName,UserPwd,Telephone,Email,IsLock,AddTime)");
            strSql.Append(" values (");
            strSql.Append("@RoleId,@RoleType,@RealName,@UserName,@UserPwd,@Telephone,@Email,@IsLock,@AddTime)");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@RoleId", MySqlDbType.Int32,11),
                    new MySqlParameter("@RoleType", MySqlDbType.Int32,11),
                    new MySqlParameter("@RealName", MySqlDbType.VarChar,20),
                    new MySqlParameter("@UserName", MySqlDbType.VarChar,50),
                    new MySqlParameter("@UserPwd", MySqlDbType.VarChar,50),
                    new MySqlParameter("@Telephone", MySqlDbType.VarChar,15),
                    new MySqlParameter("@Email", MySqlDbType.VarChar,50),
                    new MySqlParameter("@IsLock", MySqlDbType.Bit),
                    new MySqlParameter("@AddTime", MySqlDbType.DateTime)};
            parameters[0].Value = model.RoleId;
            parameters[1].Value = model.RoleType;
            parameters[2].Value = model.RealName;
            parameters[3].Value = model.UserName;
            parameters[4].Value = model.UserPwd;
            parameters[5].Value = model.Telephone;
            parameters[6].Value = model.Email;
            parameters[7].Value = model.IsLock;
            parameters[8].Value = model.AddTime;
            return DbHelperMySQL.ExecuteSql(strSql.ToString(), parameters);

        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.Manager model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update manager set ");
            strSql.Append("RoleId=@RoleId,");
            strSql.Append("RoleType=@RoleType,");
            strSql.Append("RealName=@RealName,");
            strSql.Append("UserName=@UserName,");
            strSql.Append("UserPwd=@UserPwd,");
            strSql.Append("Telephone=@Telephone,");
            strSql.Append("Email=@Email,");
            strSql.Append("IsLock=@IsLock");
            strSql.Append(" where ID=@ID ");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@RoleId", MySqlDbType.Int32,11),
                    new MySqlParameter("@RoleType", MySqlDbType.Int32,11),
                    new MySqlParameter("@RealName", MySqlDbType.VarChar,20),
                    new MySqlParameter("@UserName", MySqlDbType.VarChar,50),
                    new MySqlParameter("@UserPwd", MySqlDbType.VarChar,50),
                    new MySqlParameter("@Telephone", MySqlDbType.VarChar,15),
                    new MySqlParameter("@Email", MySqlDbType.VarChar,50),
                    new MySqlParameter("@IsLock", MySqlDbType.Bit),
                    new MySqlParameter("@ID", MySqlDbType.Int32,11)};
            parameters[0].Value = model.RoleId;
            parameters[1].Value = model.RoleType;
            parameters[2].Value = model.RealName;
            parameters[3].Value = model.UserName;
            parameters[4].Value = model.UserPwd;
            parameters[5].Value = model.Telephone;
            parameters[6].Value = model.Email;
            parameters[7].Value = model.IsLock;
            parameters[8].Value = model.ID;

            int rows = DbHelperMySQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from manager ");
            strSql.Append(" where ID=@ID ");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@ID", MySqlDbType.Int32,11)         };
            parameters[0].Value = ID;
            return DbHelperMySQL.ExecuteSql(strSql.ToString(), parameters) > 0;
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Model.Manager GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID, AddTime, RoleId, RoleType, RealName, UserName, UserPwd, Telephone, Email, IsLock  ");
            strSql.Append("  from manager ");
            strSql.Append(" where ID=@ID ");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@ID", MySqlDbType.Int32,11)         };
            parameters[0].Value = ID;

            DataTable dt = DbHelperMySQL.Table(strSql.ToString(), parameters);
            return TableHelper.DataTableToList<Model.Manager>(dt).FirstOrDefault();
        }


        /// <summary>
        /// 根据用户名密码返回一个实体
        /// </summary>
        public Model.Manager GetModel(string userName, string userPwd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from manager");
            strSql.Append(" where UserName=@UserName and UserPwd=@UserPwd and IsLock=0");
            MySqlParameter[] parameters = {
                    new MySqlParameter("@UserName", MySqlDbType.VarChar,50),
                    new MySqlParameter("@UserPwd", MySqlDbType.VarChar,50)};
            parameters[0].Value = userName;
            parameters[1].Value = userPwd;

            return PagingHelper.FirstOrDefault<Model.Manager>(strSql.ToString());
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<Model.Manager> GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from manager ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return PagingHelper.List<Model.Manager>(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public IList<Model.Manager> GetList(int top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from manager ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            if (top > 0)
            {
                strSql.AppendFormat(" limit {0}", top);
            }
            return PagingHelper.List<Model.Manager>(strSql.ToString());
        }

        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public PagedList<IList<Model.Manager>> GetPagedList(int pageSize, int pageIndex, string strWhere, string filedOrder, ManagerField field)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from manager ");
            strSql.Append(" where 1=1");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
            }
            if (!string.IsNullOrWhiteSpace(field.username))
            {
                strSql.Append($" and UserName like '%{field.username}%'");
            }

            if (!string.IsNullOrWhiteSpace(field.realname))
            {
                strSql.Append($" and RealName like '%{field.realname}%'");
            }
            if (field.roleId > 0) {
                strSql.Append($" and RoleId = {field.roleId}");
            }

            if (!string.IsNullOrWhiteSpace(field.phone))
            {
                strSql.Append($" and Telephone like '%{field.phone}%'");
            }
            //strSql.Append(" order by " + filedOrder);

            return PagingHelper.PagedList<Model.Manager>(pageSize, pageIndex, strSql.ToString(), filedOrder);
        }
    }

}
