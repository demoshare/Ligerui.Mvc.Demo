﻿using QK.Common;
using QK.DBUtility;
using QK.Model.Expand;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace QK.DAL
{
    /// <summary>  
    /// 分页帮助类
    /// </summary>  
    public static class PagingHelper
    {
        /// <summary>
        /// 获取分页SQL语句
        /// </summary>
        /// <param name="_recordCount">记录总数</param>
        /// <param name="_pageSize">每页记录数</param>
        /// <param name="_pageIndex">当前页数</param>
        /// <param name="_safeSql">SQL查询语句</param>
        /// <param name="_orderField">排序字段，多个则用“,”隔开</param>
        /// <returns>分页SQL语句</returns>
        private static string CreatePagingSql(int _recordCount, int _pageSize, int _pageIndex, string _safeSql, string _orderField)
        {
            ////重新组合排序字段，防止有错误
            //string[] arrStrOrders = _orderField.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            //StringBuilder sbOriginalOrder = new StringBuilder(); //原排序字段
            //StringBuilder sbReverseOrder = new StringBuilder(); //与原排序字段相反，用于分页
            //for (int i = 0; i < arrStrOrders.Length; i++)
            //{
            //    arrStrOrders[i] = arrStrOrders[i].Trim();  //去除前后空格
            //    if (i != 0)
            //    {
            //        sbOriginalOrder.Append(", ");
            //        sbReverseOrder.Append(", ");
            //    }
            //    sbOriginalOrder.Append(arrStrOrders[i]);

            //    int index = arrStrOrders[i].IndexOf(" "); //判断是否有升降标识
            //    if (index > 0)
            //    {
            //        //替换升降标识，分页所需
            //        bool flag = arrStrOrders[i].IndexOf(" DESC", StringComparison.OrdinalIgnoreCase) != -1;
            //        sbReverseOrder.AppendFormat("{0} {1}", arrStrOrders[i].Remove(index), flag ? "ASC" : "DESC");
            //    }
            //    else
            //    {
            //        sbReverseOrder.AppendFormat("{0} DESC", arrStrOrders[i]);
            //    }
            //}

            //计算总页数
            _pageSize = _pageSize == 0 ? _recordCount : _pageSize;
            int pageCount = (_recordCount + _pageSize - 1) / _pageSize;

            //检查当前页数
            if (_pageIndex < 1)
            {
                _pageIndex = 1;
            }
            else if (_pageIndex > pageCount)
            {
                _pageIndex = pageCount;
            }

            //拼接SQL语句
            StringBuilder strSql = new StringBuilder();

            strSql.AppendFormat(" {0} ", _safeSql);
            strSql.AppendFormat(" ORDER BY {0} ", _orderField);
            if (_recordCount > 0)
                strSql.AppendFormat(" LIMIT {0},{1} ", _pageSize * (_pageIndex - 1), _pageSize);
            else
                strSql.AppendFormat(" LIMIT {0},{1} ", 0, _pageSize);

            return strSql.ToString();
        }

        /// <summary>
        /// 获取记录总数SQL语句
        /// </summary>
        /// <param name="_safeSql">SQL查询语句</param>
        /// <returns>记录总数SQL语句</returns>
        private static string CreateCountingSql(string _safeSql)
        {
            return string.Format(" SELECT COUNT(1) AS RecordCount FROM ({0}) AS T ", _safeSql);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static T FirstOrDefault<T>(string strSql) where T : class
        {
            DataTable dt = DbHelperMySQL.Table(strSql.ToString());
            return TableHelper.DataTableToList<T>(dt).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IList<T> List<T>(string strSql) where T : class
        {
            DataTable dt = DbHelperMySQL.Table(strSql.ToString());
            return TableHelper.DataTableToList<T>(dt);
        }

        /// <summary>
        /// 数据分页
        /// </summary>
        /// <returns></returns>
        public static PagedList<IList<T>> PagedList<T>(int pageSize, int pageIndex, string strSql, string filedOrder)
              where T : class
        {
            int recordCount = Convert.ToInt32(DbHelperMySQL.GetSingle(CreateCountingSql(strSql.ToString())));
            DataTable dt = DbHelperMySQL.Table(CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            return new PagedList<IList<T>>()
            {
                Rows = TableHelper.DataTableToList<T>(dt),
                Total = recordCount
            };
        }
    }
}
